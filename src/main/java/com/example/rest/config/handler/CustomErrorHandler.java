package com.example.rest.config.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;
import java.net.URI;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * @author LiuDong
 */
@Slf4j
public class CustomErrorHandler implements ResponseErrorHandler {

    /**
     * 返回false表示不管response的status是多少都返回没有错; 这里可以自己定义哪些status code或body.message可以抛Error
     * 指示给定的响应是否有任何错误。
     * 实现通常会检查响应的HttpStatus。
     * Indicate whether the given response has any errors.
     * Implementations will typically inspect the HttpStatus of the response.
     * @param response – the response to inspect
     * @return true if the response indicates an error; false otherwise
     * @throws IOException – in case of I/O errors
     */
    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException {
        return response.getStatusCode().value() != 200 && response.getStatusCode().value() !=302;
    }

    /**
     * 处理给定响应中的错误。
     * 这个方法只在hasError(ClientHttpResponse)返回true时被调用。
     * Handle the error in the given response.
     * This method is only called when hasError(ClientHttpResponse) has returned true.
     * @param response – the response with the error
     * @throws IOException – in case of I/O errors
     */
    @Override
    public void handleError(ClientHttpResponse response) throws IOException {

    }

    /**
     * handleError(ClientHttpResponse)的替代方法，附加信息提供对请求URL和HTTP方法的访问。
     * Alternative to handleError(ClientHttpResponse) with extra information providing access to the request URL and HTTP method.
     * @param url – the request URL
     * @param method – the HTTP method
     * @param response – the response with the error
     * @throws IOException – in case of I/O errors
     */
    @Override
    public void handleError(URI url, HttpMethod method, ClientHttpResponse response) throws IOException {
        log.error("=======================ERROR============================");
        log.error("DateTime：{}", LocalDateTime.ofInstant(Instant.ofEpochMilli(response.getHeaders().getDate()), ZoneId.systemDefault()));
        log.error("HOST:{},URI：{}", url.getHost(), url.getPath());
        log.error("Method：{}", method.name());
        log.error("Exception：{}", response.getStatusCode());
        log.error("========================================================");
    }
}
