package com.example.rest.config;

import com.example.rest.config.handler.CustomErrorHandler;
import com.example.rest.config.interceptor.LogClientHttpRequestInterceptor;
import com.example.rest.config.interceptor.RequestIdInterceptor;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * RestTemplate用于同步client端的核心类，简化了与http服务的通信，并满足RestFul原则，程序代码可以给它提供URL，并提取结果
 * 默认情况下，RestTemplate默认使用jdk提供http链接的能力（HttpURLConnection）
 * 可以通过setRequestFactory属性切换到不同的HTTP源，Apache HttpComponents,Netty和OkHttp。
 * @author LiuDong
 * @since Apr 29, 2021 22:42:58 PM
 */
@Configuration
public class RestTemplateConfiguration {

    /**
     * HttpMessageConverter 对象转换器
     * ClientHttpRequestFactory  默认是jdk的HttpURLConnection
     * ResponseErrorHandler 异常处理
     * ClientHttpRequestInterceptor 请求拦截器(支持多个,@Order(int),通过InterceptingHttpAccessor#setInterceptors保证很多个Interceptors的执行顺序)
     * @param builder RestTemplateBuilder
     * @return RestTemplate
     */
    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder){
        RestTemplate restTemplate = builder
                .errorHandler(new CustomErrorHandler())
                .basicAuthentication("username","password")
                .interceptors(new RequestIdInterceptor(), new LogClientHttpRequestInterceptor())
                .build();

        //生成一个设置了连接超时时间、请求超时时间、异常最大重试次数的httpClient
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectionRequestTimeout(1000)
                .setConnectTimeout(10000)
                .setSocketTimeout(30000)
                .build();
        HttpClient httpClient = HttpClientBuilder.create()
                .setDefaultRequestConfig(requestConfig)
                .setRetryHandler(new DefaultHttpRequestRetryHandler(5, false))
                .setUserAgent("agent")
                .setMaxConnPerRoute(10)
                .setMaxConnTotal(50)
                .build();
        //使用httpClient创建一个ClientHttpRequestFactory的实现
        ClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);
        //ClientHttpRequestFactory作为参数构造一个使用作为底层的RestTemplate
        /*restTemplate.setRequestFactory(requestFactory);*/
        //ClientHttpRequestFactory此处替换为BufferingClientHttpRequestFactory
        restTemplate.setRequestFactory(new BufferingClientHttpRequestFactory(requestFactory));

        //获取RestTemplate默认配置好的所有转换器
        List<HttpMessageConverter<?>> messageConverters = restTemplate.getMessageConverters();
        //默认的MappingJackson2HttpMessageConverter在第7个 先把它移除掉
        messageConverters.remove(6);
        //添加上GSON的转换器
        messageConverters.add(6, new GsonHttpMessageConverter());
        messageConverters.add(1, new StringHttpMessageConverter(StandardCharsets.UTF_8));
        System.out.println(messageConverters);

        return restTemplate;
    }

}
