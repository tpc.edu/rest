[TOC]

# RestTemplate访问需要OAuth2授权的服务

## 概述

随着微服务安全性的增强，需要携带token才能访问其API，然而RestTemplate默认并不会将 token 放到 Header 中，那么如何使用RestTemplate实现自动设置授权信息并访问需要OAuth2授权的服务呢？

本文重点讲述如何通过`OAuth2RestTemplate`实现自动设置授权信息，并访问需要OAuth2的client模式授权的服务。需要重点理解下面两点：

* OAuth2.0配置
* OAuth2RestTemplate

> 本文依赖:
> * spring-boot-starter-parent:2.4.2
> * spring-cloud-starter-oauth2:2.2.4.RELEASE

## 示例

### OAuth2.0相关配置

#### 引入依赖

```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-oauth2</artifactId>
    <version>2.2.4.RELEASE</version>
</dependency>
```

#### 配置application.yml

``` yaml
security:
  oauth2:
    client:
      client-id: car-client
      client-secret: 123456
      grant-type: authorization_code #client_credentials
      user-authorization-uri: ${auth.service}/oauth/authorize #请求认证的地址
      access-token-uri: ${auth.service}/oauth/token #请求令牌的地址
      pre-established-redirect-uri: ${client.service}/callback
      scope:
        - all

auth.service: http://localhost:8080
client.service: http://localhost:8091
```

### OAuth2RestTemplate

#### ==编写OAuth2RestTemplateConfiguration(重点)==

```java
@Configuration
public class OAuth2RestTemplateConfiguration {

    @Resource
    private ClientCredentialsResourceDetails clientCredentialsResourceDetails;

    @Bean
    public OAuth2RestTemplate clientCredentialsRestTemplate() {
        return new OAuth2RestTemplate(clientCredentialsResourceDetails);
    }

    @Bean
    @ConfigurationProperties("security.oauth2.client")
    //@ConfigurationProperties("spring.security.oauth2.client.registration.github")
    public AuthorizationCodeResourceDetails google() {
        return new AuthorizationCodeResourceDetails();
    }

    @Bean
    public OAuth2RestTemplate authorizationCodeRestTemplate(AuthorizationCodeResourceDetails authorizationCodeResourceDetails) {
        return new OAuth2RestTemplate(clientCredentialsResourceDetails);
    }
}
```

## 拓展

RestTemplate访问需要Basic授权的服务
```java
RestTemplate restTemplate = new RestTemplate();
restTemplate.getInterceptors().add(new BasicAuthenticationInterceptor("username", "password"));
```
或者
```java
RestTemplate restTemplate = new RestTemplateBuilder().basicAuthentication("username", "password").build();
```

## 参考

* [参考文档](https://blog.csdn.net/lixiang987654321/article/details/107745998 "OAuth2授权客户端访问资源服务")